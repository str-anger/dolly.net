﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;


[assembly: AssemblyTitle("Dolly demo project")]
[assembly: AssemblyDescription("Demonstrates the core function of Dolly lib")]
[assembly: AssemblyCompany("Parallels")]
[assembly: AssemblyProduct("Dolly")]
[assembly: AssemblyCopyright("Stanislav I. Protasov, 2013")]
[assembly: ComVisible(false)]
[assembly: Guid("d7515ba4-310d-4a54-a7a6-6bcc1ff4357e")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.*")]

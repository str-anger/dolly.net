﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Dolly.Demo
{
	class Program
	{
		private static List<IEnumerable<int>> clones = new List<IEnumerable<int>>();

		private static void Writer(object idx) 
		{
			var random = new Random(DateTime.Now.Millisecond);
			var clone = clones[(int)idx];
			foreach (var value in clone)
			{
				Console.WriteLine("Thread {0} value {1}", idx, value);
				Thread.Sleep(random.Next(40) + 200);
			}
		}
		
		static void Main(string[] args)
		{
			var enumerable = Enumerable.Range(0, 50);
			ICloner<int> cloner = new CloneFactory<int>(enumerable);
			var threads = new List<Thread>();
			for (int i = 0; i < 20; i++)
			{
				clones.Add(cloner.GetClone());
				threads.Add(new Thread(Writer));
			}
			cloner.AllowReading();
			for (int i = 0; i < 20; i++) threads[i].Start(i);
		}
	}
}

﻿using System.Collections.Generic;

namespace Dolly
{
	public interface ICloner<T>
	{
		/// <summary>
		/// Gets the copy of IEnumerable
		/// </summary>
		/// <returns>a copy of IEnumerable</returns>
		IEnumerable<T> GetClone();


		/// <summary>
		/// Allows to start reading from cloned enumerables. Call it when all readers registered.
		/// </summary>
		void AllowReading();
	}
}

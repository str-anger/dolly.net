﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Dolly")]
[assembly: AssemblyDescription("Cloning library for synchronous usage of collections.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Parallels")]
[assembly: AssemblyProduct("Dolly")]
[assembly: AssemblyCopyright("Stanislav I. Protasov, 2013")]
[assembly: ComVisible(false)]
[assembly: Guid("a9da5dd3-2cb3-4d2c-bdeb-4822dc381aea")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]

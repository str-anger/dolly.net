﻿using System.Collections.Generic;
using System.Linq;

namespace Dolly.Tools
{
	public class MarkovEnumerable
	{
		/// <summary>
		/// Creates chains with "Markov" property: it contains trace of N earlier entries.
		/// </summary>
		/// <typeparam name="T">collection type</typeparam>
		/// <param name="ienum">source collection</param>
		/// <param name="depth">exact element [0] and predecessors from [1st] to [last = depth-1] </param>
		/// <returns>T[depth] array of [i, i-1, i-2, ..., i - depth + 1]</returns>
		public static IEnumerable<T[]> DeepMarkov<T>(IEnumerable<T> ienum, int depth)
		{
			Queue<T> q = new Queue<T>(depth);

			foreach (var element in ienum)
			{
				q.Enqueue(element);
				if (q.Count() == depth)
				{
					yield return q.Reverse().ToArray();
					q.Dequeue();
				}
			}
		} 
	}
}

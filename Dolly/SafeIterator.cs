﻿using System;
using System.Collections.Generic;

namespace Dolly
{
	public static class SafeIterator
	{
		public static void SafeForEach<T>(this IEnumerable<T> ie, Action<T> action) 
		{
			foreach (var i in ie)
				try	{ action(i); } catch { }
		}

		public static IEnumerable<TOut> SafeSelect<T, TOut>(this IEnumerable<T> ie, Func<T, TOut> func) 
		{
			foreach (var i in ie)
			{
				TOut t = default(TOut);
				bool ok = true;
				try { t = func(i); } catch { ok = false; }
				if (ok) yield return t;
			}
		}

	}
}

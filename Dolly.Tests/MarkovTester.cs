﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dolly.Tests
{
	[TestClass]
	public class MarkovTester
	{
		[TestMethod]
		public void Markov()
		{
			IEnumerable<int> ie = Enumerable.Range(0, 7);
			var iem = Tools.MarkovEnumerable.DeepMarkov(ie, 6).ToArray();
			Assert.AreEqual(2, iem.Length);
			Assert.AreEqual(6, iem[0].Length);
			Assert.AreEqual(5, iem[0][0]);
			Assert.AreEqual(0, iem[0].Last());
			Assert.AreEqual(1, iem[1].Last());
		}
	}
}

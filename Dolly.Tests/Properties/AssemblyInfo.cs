﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Dolly test project")]
[assembly: AssemblyDescription("Demonstrates the core function of Dolly lib")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Parallels")]
[assembly: AssemblyProduct("Dolly")]
[assembly: AssemblyCopyright("Stanislav I. Protasov, 2013")]
[assembly: ComVisible(false)]
[assembly: Guid("16cbe860-3bf6-4dd9-9f90-716466e26c14")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.*")]
